source('/home/admin/CODE/common/aggregate.R')

METERACNAMESAGG = c("GDC_MFM","MAR_MFM","GDC_Inverter_1","GDC_Inverter_2","GDC_Inverter_3","GDC_Inverter_4","GDC_Inverter_5","GDC_Inverter_6","GDC_Inverter_7","GDC_Inverter_8","GDC_Inverter_9","GDC_Inverter_10","MAR_Inverter_1","MAR_Inverter_2","MAR_Inverter_3", "MAR_Inverter_4","MAR_Inverter_5","MAR_Inverter_6")

registerMeterList("IN-004C",METERACNAMESAGG)
for( x in 1 : length(METERACNAMESAGG))
{
		aggNameTemplate = getNameTemplate()
		aggColTemplate = getColumnTemplate()
		{
		if(grepl("WMS",METERACNAMESAGG[x]))
		{
			aggColTemplate[1] = 1 #Column no for date
			aggColTemplate[2] = 2 #Column no for DA
			aggColTemplate[3] = NA #column for LastRead
			aggColTemplate[4] = NA #column for LastTime
			aggColTemplate[5] = NA #column for Eac-1
			aggColTemplate[6] = NA #column for Eac-2
			aggColTemplate[7] = NA #column for Yld-1
			aggColTemplate[8] = NA #column for Yld-2
			aggColTemplate[9] = NA #column for PR-1
			aggColTemplate[10] = NA #column for PR-2
			aggColTemplate[11] = 3 #column for Irr
			aggColTemplate[12] = "Self" # IrrSrc Value
			aggColTemplate[13] = 4 #column for Tamb
			aggColTemplate[14] = 5 #column for Tmod
			aggColTemplate[15] = NA #column for Hamb
		}
		else if(grepl("MFM",METERACNAMESAGG[x]))
		{
			aggColTemplate[1] = 1 #Column no for date
			aggColTemplate[2] = 2 #Column no for DA
			aggColTemplate[3] = 9 #column for LastRead
			aggColTemplate[4] = 10 #column for LastTime
			aggColTemplate[5] = 3 #column for Eac-1
			aggColTemplate[6] = 4 #column for Eac-2
			aggColTemplate[7] = 5 #column for Yld-1
			aggColTemplate[8] = 6 #column for Yld-2
			aggColTemplate[9] = 7 #column for PR-1
			aggColTemplate[10] = 8 #column for PR-2
			aggColTemplate[11] = NA #column for Irr
			aggColTemplate[12] = "IN-711S-WMS" # IrrSrc Value
			aggColTemplate[13] = NA #column for Tamb
			aggColTemplate[14] = NA #column for Tmod
			aggColTemplate[15] = NA #column for Hamb
			#aggColTemplate[16] = NA #column for IA
			#aggColTemplate[17] = 11 #column for GA
			#aggColTemplate[18] = 12 #column for PA
		}
		else if(grepl("Inverter",METERACNAMESAGG[x]))
		{
			aggColTemplate[1] = 1 #Column no for date
			aggColTemplate[2] = 2 #Column no for DA
			aggColTemplate[3] = 8 #column for LastRead
			aggColTemplate[4] = 9 #column for LastTime
			aggColTemplate[5] = 3 #column for Eac-1
			aggColTemplate[6] = 4 #column for Eac-2
			aggColTemplate[7] = 6 #column for Yld-1
			aggColTemplate[8] = 7 #column for Yld-2
			aggColTemplate[9] = NA #column for PR-1
			aggColTemplate[10] = NA #column for PR-2
			aggColTemplate[11] = NA #column for Irr
			aggColTemplate[12] = NA # IrrSrc Value
			aggColTemplate[13] = NA #column for Tamb
			aggColTemplate[14] = NA #column for Tmod
			aggColTemplate[15] = NA #column for Hamb
			#aggColTemplate[16] = 10 #column for IA
		}
		}
		registerColumnList("IN-004C",METERACNAMESAGG[x],aggNameTemplate,aggColTemplate)
}
