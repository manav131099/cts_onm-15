import datetime as DT
import requests
import os
from lxml import etree
import pandas as pd
import csv
from datetime import timedelta 
import shutil

site = "DEWAS" #Enter site name

start = ['2020-01-01','2020-02-01','2020-03-01','2020-04-01','2020-05-01','2020-06-01','2020-07-01','2020-08-01','2020-09-01','2020-10-01'] #enter first date of every month
end = ['2020-01-31','2020-02-29','2020-03-31','2020-04-30','2020-05-31','2020-06-30','2020-07-31','2020-08-31','2020-09-30','2020-10-16'] # enter last date of every month

api_key = 'HesHutsorc8mnk95sc'
url = 'https://solargis.info/ws/rest/datadelivery/request?key=%s' % api_key
headers = {'Content-Type': 'application/xml'}
xmlfile = open('/home/admin/CODE/GIS_API2/API_req.xml', 'r')
body = xmlfile.read()

for (a, b) in zip(start, end): 
      response = requests.post(url, data=body.format(Start_Date= a, End_Date = b, latitude= "22.938423" , longitude = "76.034573", tilt = "20", azimuth = "180", para = "GHI GTI"), headers=headers) 
      data = []
      ghi_rounded = []
      gti_rounded = []
      ts_rounded = []
      root = etree.fromstring(bytes(response.text, encoding='utf-8'))
      for element in root.iter("*"):
          data.append(element.items())
      tuple_list = [item for t in data for item in t] 
      first_tuple_elements = []
      
      for a_tuple in tuple_list:
          first_tuple_elements.append(a_tuple[1])
      #print(first_tuple_elements)
      ts = first_tuple_elements[1::2]
      for i in ts:
        x = slice(0, 10)
        ts_rounded.append(i[x])
      
      ghi = first_tuple_elements[2::2]
      ghi.pop(0)
      for i in ghi:
        i = i.split()
      
        i[0] = float(i[0])
        i[1] = float(i[1])
        i[0] = round(i[0],2)
        i[1] = round(i[1],2)
        ghi_rounded.append(i[0])
        gti_rounded.append(i[1])
      list_of_tuples = list(zip(ts_rounded, ghi_rounded)) 
      df = pd.DataFrame(list_of_tuples) 
      df = df.iloc[1:]
      ts_rounded.pop(0)
      file = open("/home/admin/Dropbox/GIS_API2/"+site+"/"+site+"_AGGREGATE.txt", "a")
      for index in range(len(ts_rounded)):
          file.write(str(ts_rounded[index]) + "\t" + str(ghi_rounded[index]) +  "\t" + str(gti_rounded[index]) + "\n")
      file.close() 

          