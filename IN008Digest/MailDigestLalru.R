errHandle = file('/home/admin/Logs/LogsLalruMail.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)
source('/home/admin/CODE/IN008Digest/HistoricalAnalysis2G3GLalru.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
source('/home/admin/CODE/Misc/memManage.R')
source('/home/admin/CODE/MasterMail/timestamp.R')
source('/home/admin/CODE/common/math.R')
source('/home/admin/CODE/IN008Digest/aggregateInfo.R')
YldRecorded = c(0,0)
DAYFULLSITE = ""
initDigest = function(df,no,extra)
{
  body = "\n\n_____________________________________________\n\n"
  body = paste(body,as.character(df[,1]),"Meter",no)
  DAYFULLSITE <<- as.character(df[,1])
	body = paste(body,"\n\n_____________________________________________\n\n")
  body = paste(body,"Eac",no,"-1 [kWh]: ",as.character(df[,2]),sep="")
  body = paste(body,"\n\nEac",no,"-2 [kWh]: ",as.character(df[,3]),sep="")
	body = paste(body,"\n\nRatio Eac",no,"-2/Eac",no,"-1: ",round((as.numeric(df[,3])/as.numeric(df[,2]))*100,2),sep="")
  acpts = round(as.numeric(df[,4]) * 2.88)
  body = paste(body,"\n\nPoints recorded: ",acpts," (",as.character(df[,4]),"%)",sep="")
  body = paste(body,"\n\nDowntime (%):",as.character(df[,5]))
  body = paste(body,"\n\nYield-1 [kWh/kWp]:",as.character(df[,6]))
  body = paste(body,"\n\nYield-2 [kWh/kWp]:",as.character(df[,7]))
	YldRecorded[no] <<- as.numeric(df[,7])
  body = paste(body,"\n\nLast recorded timestamp:",extra[1])
  body = paste(body,"\n\nLast recorded energy meter reading [kWh]:",extra[2])
  return(body)
}
printtsfaults = function(TS,num,body)
{
	if(length(TS) > 1)
	{
		body = paste(body,"\n\n_____________________________________________\n\n")
		body = paste(body,paste("Timestamps for Meter",num,"where Pac < 30 between 8am - 6pm\n\n"))
		for(lm in  1 : length(TS))
		{
			body = paste(body,TS[lm],sep="\n")
		}
	}
	return(body)
}
sendMail = function(df1,df2,pth1,pth2,pth3,extra)
{
  filetosendpath = c(pth1,pth2)
  if(file.exists(pth3))
	{
	filetosendpath = c(pth1,pth2,pth3)
  }
	filenams = c()
  for(l in 1 : length(filetosendpath))
  {
    temp = unlist(strsplit(filetosendpath[l],"/"))
    filenams[l] = temp[length(temp)]
		if(l == 1)
			currday = temp[length(temp)]
  }
	print('Filenames Processed')
	body = "Site Name: ALP Lalru"
	body = paste(body,"Location: Punjab, India",sep="\n\n")
	body = paste(body,"O&M Code: IN-008",sep="\n\n")
	body = paste(body,"System Size: 510.125",sep="\n\n")
	body = paste(body,"Number of Energy Meters: 2",sep="\n\n")
	body = paste(body,"Capacity Meter-1: 291.5",sep="\n\n")
	body = paste(body,"Capacity Meter-2: 218.625",sep="\n\n")
	body = paste(body,"Module Brand / Model / Nos: Canadian Solar / 265W / 1925",sep="\n\n")
	body = paste(body,"Inverter Brand / Model / Nos: SMA / STP 60 / 07",sep="\n\n")
	body = paste(body,"Site COD: 2016-02-23",sep="\n\n")
	body = paste(body,"\n\nDays alive: ",DAYSALIVE + 50,sep="")
	body = paste(body,"\n\nYears alive: ",format(round((DAYSALIVE + 50)/365,2),nsmall=2),sep="")
	bodyac = body
  body = initDigest(df1,1,extra[1:2])
  body = printtsfaults(TIMESTAMPSALARM,1,body)
	body = paste(body,initDigest(df2,2,extra[3:4]))
  body = printtsfaults(TIMESTAMPSALARM2,2,body)
	stddevvals = round(sdp(as.numeric(YldRecorded)),2)
	covvals = round((stddevvals*100 / mean(YldRecorded)),1)
	bodyac = paste(bodyac,"\n\nStdev yield: ",stddevvals,sep="")
	bodyac = paste(bodyac,"\n\nCOV yield (%): ",covvals,sep="")
	print('2G data processed')
	bodyac = paste(bodyac,"\n\n_____________________________________________\n\n")
	bodyac = paste(bodyac,DAYFULLSITE,"FULL SITE")
	bodyac = paste(bodyac,"\n\n_____________________________________________\n\n")
	eacfs1 = (as.numeric(df1[,2])+as.numeric(df2[,2]))
	eacfs2 = (as.numeric(df1[,3])+as.numeric(df2[,3]))
	bodyac = paste(bodyac,"Eac Total using method 1:",eacfs1)
	bodyac = paste(bodyac,"\n\nEac Total using method 2:",eacfs2)
	bodyac = paste(bodyac,"\n\nYield-1 full-site [kWh/kWp]:",(round(eacfs1/510.125,2)))
	bodyac = paste(bodyac,"\n\nYield-2 full site [kWh/kWp]:",(round(eacfs2/510.125,2)))
	body = paste(bodyac,body,sep="")
  body = paste(body,"\n\n_____________________________________________\n\n")
  body = paste(body,"Station History")
  body = paste(body,"\n\n_____________________________________________\n\n")
  body = paste(body,"Station DOB:",as.character(DOB))
  body = paste(body,"\n\n# Days alive:",as.character(DAYSALIVE))
  yrsalive = format(round((DAYSALIVE/365),2),nsmall=2)
  body = paste(body,"\n\n# Years alive:",yrsalive)
  body = paste(body,"\n\n# Eac1 total this month [kWh]:",MONTHTOTEAC1)
  body = paste(body,"\n\n# Eac2 total this month [kWh]:",MONTHTOTEAC2)
  proje1 = format(round((MONTHTOTEAC1 * DAYSTHISMONTHAC / DAYSTHISMONTH),1),nsmall=1)
  proje2 = format(round((MONTHTOTEAC2 * DAYSTHISMONTHAC / DAYSTHISMONTH),1),nsmall=1)
  body = paste(body,"\n\n# Eac1 projected total for the month [kWh]:",proje1)
  body = paste(body,"\n\n# Eac2 projected total for the month [kWh]:",proje2)
	if(file.exists(pth3))
	{
	df = read.table(pth3,header =T,sep="\t")
	avge1 = format(round(mean(as.numeric(df[,6])),1),nsmall=1)
  avge2 = format(round(mean(as.numeric(df[,7])),1),nsmall=1)

  body = paste(body,"\n\n# Eac1 average for the month [kWh]:",avge1)
  body = paste(body,"\n\n# Eac2 average for the month [kWh]:",avge2)
  }
	body = paste(body,"\n\n# Eac1 sum last 30 days [kWh]:",sum(LAST30DAYSEAC1))
  body = paste(body,"\n\n# Eac2 sum last 30 days [kWh]:",sum(LAST30DAYSEAC2))
  body = paste(body,"\n\n# Eac1 average last 30 days [kWh]:",round(mean(LAST30DAYSEAC1),1))
  body = paste(body,"\n\n# Eac2 average last 30 days [kWh]:",round(mean(LAST30DAYSEAC2),1))
  body = paste(body,"\n\nEac1/Eac2 lifetime ratio :",round((EAC1GLOB/EAC2GLOB),4))
	print('3G data processed')
	body = gsub("\n ","\n",body)
  send.mail(from = sender,
            to = recipients,
            subject = paste("Station [IN-008X] Digest",substr(currday,14,23)),
            body = body,
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
            authenticate = TRUE,
            send = TRUE,
            attach.files = filetosendpath,
            file.names = filenams, # optional parameter
            debug = F)
	recordTimeMaster("IN-008X","Mail",substr(currday,14,23))
}
sender = c('operations@cleantechsolar.com')
uname = 'shravan.karthik@cleantechsolar.com'
recipients = getRecipients("IN-008X","m")
#recipients = c('shravan1994@gmail.com')

pwd = 'CTS&*(789'
todisp = 1
while(1)
{
	recipients = getRecipients("IN-008X","m")
  recordTimeMaster("IN-008X","Bot")
	sendmail = 0
  prevx = x
  prevy = y
  prevt = t
  years = dir(path)
  for(x in prevx : length(years))
  {
    pathyear = paste(path,years[x],sep="/")
    writepath2Gyr = paste(writepath2G,years[x],sep="/")
    writepath3Gyr = paste(writepath3G,years[x],sep="/")
    checkdir(writepath2Gyr)
    checkdir(writepath3Gyr)
    months = dir(pathyear)
    startmnth = 1
    if(prevx == x)
    {
      startmnth = prevy
    }
    for(y in startmnth : length(months))
    {
      pathmonths = paste(pathyear,months[y],sep="/")
      writepath2Gmon = paste(writepath2Gyr,months[y],sep="/")
      writepath3Gfinal = paste(writepath3Gyr,"/[IN-008X] ",months[y],".txt",sep="")
      checkdir(writepath2Gmon)
      stations = dir(pathmonths)
      pathdays = paste(pathmonths,stations[1],sep="/")
      pathdays2 = paste(pathmonths,stations[2],sep="/")
      writepath2Gdays = paste(writepath2Gmon,stations[1],sep="/")
      writepath2Gdays2 = paste(writepath2Gmon,stations[2],sep="/")
      checkdir(writepath2Gdays)
      checkdir(writepath2Gdays2)
      days = dir(pathdays)
      days2 = dir(pathdays2)
      startdays = 1
      if(y == prevy)
      {
        startdays = prevt
      }
      if(startdays == 1)
      {
        MONTHTOTEAC1 = 0
        MONTHTOTEAC2 = 0
        DAYSTHISMONTH = 0
        temp = unlist(strsplit(days[1]," "))
				print(substr(temp[2],1,10))
        temp = as.Date(substr(temp[2],1,10),"%Y-%m-%d")
        DAYSTHISMONTHAC = monthDays(temp)
      }
      for(t in startdays : length(days))
      {
         condn1  = (t != length(days))
         condn2 = ((t == length(days)) && ((y != length(months)) || (x != length(years))))
         if(!(condn1 || condn2))
         {
				 	 if(todisp)
					 {
					 	print('No new files')
						todisp = 0
					 }
					 print("Hitting sleep....")
           Sys.sleep(3600)
           next
         }
				 todisp = 1
         sendmail = 1
         DAYSALIVE = DAYSALIVE + 1
				 print(paste('Processing',days[t]))
				 print(paste('Processing',days2[t]))
         writepath2Gfinal = paste(writepath2Gdays,"/",days[t],sep="")
         writepath2Gfinal2 = paste(writepath2Gdays2,"/",days2[t],sep="")
         readpath = paste(pathdays,days[t],sep="/")
				 lastdata = read.table(readpath,header = T,sep = "\t")
				 lastdata = c(as.character(lastdata[nrow(lastdata),1]),as.character(lastdata[nrow(lastdata),2]))
         readpath2 = paste(pathdays2,days2[t],sep="/")
				 dataprev = read.table(readpath2,header = T,sep = "\t")
				 lastdata[3] = as.character(dataprev[nrow(dataprev),1])
				 lastdata[4] = as.character(dataprev[nrow(dataprev),2])
				 METERCALLED <<- 1
         df1 = secondGenData(readpath,writepath2Gfinal)
				 METERCALLED <<- 2
         df2 = secondGenData(readpath2,writepath2Gfinal2)
         tots = thirdGenData(writepath2Gfinal,writepath2Gfinal2,writepath3Gfinal)
         LAST30DAYSEAC1[[idxvec]] = tots[1]
         LAST30DAYSEAC2[[idxvec]] = tots[2]
         MONTHTOTEAC1 = MONTHTOTEAC1 + tots[1]
         MONTHTOTEAC2 = MONTHTOTEAC2 + tots[2]
         DAYSTHISMONTH = DAYSTHISMONTH + 1
         EAC1GLOB = EAC1GLOB + tots[1]
         EAC2GLOB = EAC2GLOB + tots[2]
         idxvec = (idxvec + 1) %% 30
         if(idxvec == 0)
         {
           idxvec = 1
         }
  if(sendmail ==0)
  {
    next
  }
	print('Sending Mail')
  sendMail(df1,df2,writepath2Gfinal,writepath2Gfinal2,writepath3Gfinal,lastdata)
	print('Mail Sent')

      }
    }
  }
	gc()
}
sink()
