rm(list = ls())

checkPath = function(x)
{
	retVal = F
	if((!is.na(x))&&file.exists(x))
	{
		print(paste(x,"exists"))
		retVal = T
	}
	else
		print(paste(x,"doesn't exist"))
	return(retVal)
}

findExceptions = function()
{
	cmd = "cd '/home/admin/Dropbox/Third Gen'; find . | grep -v 'txt' > /tmp/cols.txt"
	system(cmd)
	data = readLines("/tmp/cols.txt")
	len = length(data)
	exceptions = c()
	idx = 1
	for(x in 1 : len)
	{
		splittext = unlist(strsplit(data[x],"/"))
		if(length(splittext)<4)
			next
		exceptions[idx] = splittext[2]
		idx = idx + 1
	}
	exceptions = unique(exceptions)
	return(exceptions)
}

findfullPath = function(stnId,date,substn,type)
{
	basePath = "/home/admin/Dropbox"
	stnType = substr(stnId,nchar(stnId),nchar(stnId))
	acStnType = stnType
	if(stnType == "E")
		stnType = "X"
	if(stnType == "C")
	{
		basePath = paste(basePath,"FlexiMC_Data",sep="/")
	}
	if(type == "2G" || (stnType=="S" && type == "3G"))
	{
		path2G = paste(basePath,"Second Gen",sep="/")
		if(stnType == "C")
			path2G = paste(basePath,"Second_Gen",sep="/")
		path2G = paste(path2G,"/[",stnId,"]",sep="")
		if(acStnType == "E")
			path2G = paste(path2G,"/",substn,sep="")
		path2G = paste(path2G,substr(date,1,4),substr(date,1,7),sep="/")
		if((!(is.na(substn) || substn == "")) && acStnType != "E")
			path2G = paste(path2G,substn,sep="/")
		if(stnType == "S" || stnType == "T" || stnType == "X")
		{
			days = dir(path2G)
			stnnickname = unlist(strsplit(days[1]," "))
			stnnickname = stnnickname[1]
			path2GMn = path2G
			path2G = paste(path2G,"/",stnnickname," ",date,".txt",sep="")
			if(type == "3G" && stnType == "S")
				path2G = paste(path2GMn,"/",stnnickname," ",substr(date,3,4),substr(date,6,7),".txt",sep="")
			return(path2G)
		}
		if(stnType == "C")
		{
			days = dir(path2G)
			stnnickname = unlist(strsplit(days[1],"-"))
			stnnickname = paste(stnnickname[1],stnnickname[2],stnnickname[3],sep="-")
			path2G = paste(path2G,"/",stnnickname,"-",date,".txt",sep="")
			return(path2G)
		}
	}
	
	if(type == "3G")
	{
		if(stnType == "T" || stnType == "X")
		{
			path3G = paste(basePath,"Third Gen",sep="/")
			path3G = paste(path3G,"/[",stnId,"]",sep="")
			if(stnId == "SG-003E")
				path3G = paste(path3G,substn,sep="/")
			path3G = paste(path3G,substr(date,1,4),sep="/")
			if(stnId != "SG-003E" && is.finite(match(paste("[",stnId,"]",sep=""),exceptions))) 
				path3G = paste(path3G,substn,sep="/")
			if(stnType == "T")
				path3G = paste(path3G,"/",substr(date,1,7),".txt",sep="")
			else
			{
				if(acStnType == "E")
					path3G = paste(path3G,"/[",stnId,"-",substr(substn,1,1),"] ",substr(date,1,7),".txt",sep="")
				else
					path3G = paste(path3G,"/[",stnId,"] ",substr(date,1,7),".txt",sep="")
			}
			return(path3G)
		}
		if(stnType == "C")
		{
			path3G = paste(basePath,"Third_Gen",sep="/")
			path3G = paste(path3G,"/[",stnId,"]",sep="")
			path3G = paste(path3G,substn,sep="/")
			path3G = paste(path3G,"/",substr(date,1,7),".txt",sep="")
			return(path3G)
		}
	}

	if(type == "4G")
	{
		if(stnType == "C")
		{
			path4G = paste(basePath,"/Fourth_Gen/[",stnId,"]",sep="")
			path4G = paste(path4G,"/[",stnId,"]-lifetime.txt",sep="")
			return(path4G)
		}
		if(stnType == "X" && (stnId == "MY-001X" || stnId == "MY-003X"))
		{
			path4G = paste(basePath,"/Fourth_Gen/[",stnId,"]",sep="")
			path4G = paste(path4G,"/[",stnId,"]-lifetime.txt",sep="")
			return(path4G)
		}
		if(acStnType == "E")
		{
			path4G = paste(basePath,"/Fourth_Gen/[",stnId,"]/",substn,sep="")
			path4G = paste(path4G,"/[",stnId,"-",substr(substn,1,1),"]-lifetime.txt",sep="")
			return(path4G)
		}
		if(stnType == "S" || stnType == "T" || stnType == "X")
			return(NA)
	}
}

testSuite = function()
{
	date = "2018-10-01"
	type = c("2G","3G","4G")

	stns = c("IN-001T","IN-004T","IN-006T","IN-008X","IN-010X","IN-047T","IN-023C","SG-003E","KH-006X","PH-003X","SG-003X","IN-035C")
	substns = c("","Maruti","ER&D","Meter 2","","","MFM","Preliminary-Settlements","Sensors","","Meter-A","Inverter_2")
	for(x in 1 : length(type))
	{
		for(y in 1:length(substns))
			ret = checkPath(findfullPath(stns[y],date,substns[y],type[x]))
	}
}

exceptions = findExceptions()
patchPath = "/home/admin/Dropbox/ErrataFix/PatchExtra.txt"

if(file.exists(patchPath))
{
	dataPatch = read.table(patchPath,header =T,sep=",",stringsAsFactors=F)
	for(x in 1 : nrow(dataPatch))
	{
		origFileExists = 1
		stnId = as.character(dataPatch[x,2])
		date = as.character(dataPatch[x,1])
		subMeter = as.character(dataPatch[x,3])
		columnName = as.character(dataPatch[x,4])
		newVal = as.character(dataPatch[x,5])
		path2G = findfullPath(stnId,date,subMeter,"2G")
		path3G = findfullPath(stnId,date,subMeter,"3G")
		path4G = findfullPath(stnId,date,subMeter,"4G")
		print(path3G)
		print(path4G)
		{
		if(!checkPath(path2G))
		{
			print("######## FILE DOESNT EXIST############")
			origFileExists = 0
			path2Gdates = unlist(strsplit(path2G,"/"))
			path2Gdates = path2Gdates[-length(path2Gdates)]
			pathsample = paste(path2Gdates,collapse="/")
			usedays = dir(pathsample)
			dataread = read.table(paste(pathsample,usedays[1],sep="/"),header=T,sep="\t",stringsAsFactors=F)
			for(t in 1 : ncol(dataread))
			{
				dataread[1,t]=NA
			}
			colOg = colnames(dataread)
			idxmtch = match(columnName,colOg)
			dataread[1,1] = date
			dataread[1,idxmtch] = newVal
			print(paste(idxmtch,"is the 2G column no"))
			write.table(dataread,file=path2G,row.names=F,col.names=F,sep="\t",append=F)
		}
		else
		{
			dataread = read.table(path2G,sep="\t",header=T,stringsAsFactors=F)
			colnames = colnames(dataread)
			idxmtch = match(columnName,colnames)
			print(paste(idxmtch,"is the 2G column no"))
			if(is.finite(idxmtch))
				dataread[1,idxmtch] = newVal
			write.table(dataread,file=path2G,row.names=F,col.names=T,sep="\t",append=F)
		}
		}
		if(checkPath(path3G))
		{
			print("%%%%%% Exists")
			dataread = read.table(path3G,sep="\t",header=T,stringsAsFactors=F)
			colnames = colnames(dataread)
			idxmtch = match(date,as.character(dataread[,1]))
			idxmtch2 = match(columnName,colnames)
			if(!is.finite(idxmtch))
				idxmtch = nrow(dataread) + 1
			print(paste(idxmtch,"is the 3G column no idxmtch"))
			print(paste(idxmtch2,"is the 3G column no idxmtch2"))
		#	if(is.finite(idxmtch))
		#		dataread[1,idxmtch] = newVal
			if(is.finite(idxmtch) && is.finite(idxmtch2))
				dataread[idxmtch,idxmtch2] = newVal
			write.table(dataread,file=path3G,row.names=F,col.names=T,sep="\t",append=F)
		}
		if(checkPath(path4G))
		{
			dataread = read.table(path4G,sep="\t",header=T,stringsAsFactors = F)
			colnames = colnames(dataread)
			idxmtch = match(date,as.character(dataread[,1]))
			idxmtch2 = match(paste(subMeter,columnName,sep="."),colnames)
			print(paste(idxmtch,"is the 4G column no idxmtch"))
			print(paste(idxmtch2,"is the 4G column no idxmtch2"))
			tmNow = as.character(format(Sys.time(),tz = "Singapore"))
			if(is.finite(idxmtch) && is.finite(idxmtch2))
			{
				if(is.finite(as.numeric(newVal)))
					newVal = as.numeric(newVal)
				dataread[idxmtch,idxmtch2] = newVal
				dataread[idxmtch,(ncol(dataread)-1)] = tmNow
			}
			write.table(dataread,file=path4G,row.names=F,col.names=T,sep="\t",append=F)
		}
		colnames = colnames(dataread)
	}
}
