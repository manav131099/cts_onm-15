rm(list = ls())
source('/home/admin/CODE/IN047Digest/Functions.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
require('mailR')
errHandle = file('/home/admin/Logs/LogsIN047Gen1.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)
timetomins = function(x)
{
	splitup = unlist(strsplit(x,":"))
	hr = as.numeric(splitup[1])
	min = as.numeric(splitup[2])
	return(((hr * 60)+min+1))
}
timetomins2 = function(x)
{
	lists = unlist(strsplit(x,"\\ "))
	seq1 = seq(from = 2, to = length(lists),by=2)
	lists = lists[seq1]
	lists = unlist(strsplit(lists,":"))
	seq1 = seq(from = 1, to = length(lists),by = 2)
	seq2 = seq(from = 2, to = length(lists),by = 2)
	hr = as.numeric(lists[seq1])
	min = as.numeric(lists[seq2])
	return((hr * 60)+min+1)
}
sendEmailToo = function(message,subj)
{
sender = c('operations@cleantechsolar.com')
uname = 'shravan.karthik@cleantechsolar.com'
recipients = getRecipients("IN-047T","a")
pwd = 'CTS&*(789'
  send.mail(from = sender,
            to = recipients,
            subject = paste(subj),
            body = message,
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
            authenticate = TRUE,
            send = TRUE,
            debug = F)

}
stationno = "[IN-047T]"
pathlastday = "/home/admin/Start/IN047T.txt"
pathraw = paste("/home/admin/Data/TORP Data",stationno,sep="/")
pathgen1 = paste("/home/admin/Dropbox/Gen 1 Data",stationno,sep="/")
monthsno = c("01","02","03","04","05","06","07","08","09","10","11","12")
daysmonthsno = c(31,28,31,30,31,30,31,31,30,31,30,31)
doneforthedayfiringpac = 0
lastfiredtspac = " "
lastsuccesspacts = " "
#req = httr::POST("http://52.70.243.223/torp/ServiceRouter/login?loginid=operations@cleantechsolar.com&pwd=torp1227")
firetwilio = function(day)
{
	if(doneforthedayfiringpac)
	{
		print(paste('Done for the day firing both twilio',day))
		return()
	}
	dataread = read.table(day,header = T,sep="\t")
	if(nrow(dataread) <  1)
	return()
	datareadac = dataread
	tmmins = timetomins2(as.character(dataread[,1]))
	dataread2 = dataread[tmmins>540,]
	tmmins = tmmins[tmmins>540]
	dataread = dataread2[tmmins < 960,]
	if(nrow(dataread)<1)
	{
		print(paste('Not yet time...Last time:',as.character(datareadac[nrow(datareadac),1])))
		return()
	}
	fulldataread = dataread
	if(lastsuccesspacts != " ")
	{
		print(paste('to match',lastsuccesspacts,'with',as.character(dataread[nrow(dataread),1])))
		idxtsmtch = match(lastsuccesspacts,as.character(dataread[,1]))
		print(paste('match val of idxts is',idxtsmtch))
		print(paste('nrow dataread',nrow(dataread)))
		if(!is.finite(idxtsmtch))
		{
			lastsuccesspacts <<- as.character(dataread[nrow(dataread),1])
			return()
		}
		if((idxtsmtch+1) > nrow(dataread))
		{
			print('No new data recorded.... returning twilio call')
			return()
		}
		dataread = dataread[(idxtsmtch+1):nrow(dataread),]
	}
	lastsuccesspacts <<- as.character(dataread[nrow(dataread),1])
	print(paste('Last timestamp recordered sucess',lastsuccesspacts))
	pac = pacac = as.numeric(dataread[,2])
	pac = pac[complete.cases(pac)]
	datareadtrue = dataread[complete.cases(pacac),]
	pacneg = pacac[!is.finite(pacac)]
	if(!doneforthedayfiringpac)
	{
	print(paste('length pacneg',length(pacneg)))
	{
		if(length(pacneg) > 0)
		{
			idx = match(pacneg[1],pacac)
			print(paste(idx,'idx val in pacneg'))
			message = paste("Station IN-047T Pac meter error, Pac reading:",as.character(pacneg[1]),"Timestamp:",as.character(dataread[idx,1]))
			command = paste('python /home/admin/CODE/Send_mail/twilio_alert.py',' "',message,'"',' "IN-047T"',sep = "")
			system(command)
			recordTimeMaster("IN-047T","TwilioAlert",as.character(dataread[idx,1]))
			sendEmailToo(message,"IN-047T Meter error")
			doneforthedayfiringpac <<-1
			print(paste('Pac NA error, message fired, time',as.character(dataread[idx,1])))
			lastfiredtspac <<- Sys.time()
		}
		else
		{
			dataread = dataread[complete.cases(pacac),]
			pac2 = pac[pac < 0.5]
			print(paste('pac2 length',length(pac2)))
			if(length(pac2) > 0)
			{
				idx = match(pac2[1],pac)
			print(paste(idx,'idx val in pac2'))

				message = paste("Station IN-047T Pac meter error, Pac reading:",as.character(pac2[1]),"Timestamp:",as.character(dataread[idx,1]))
				command = paste('python /home/admin/CODE/Send_mail/twilio_alert.py',' "',message,'"',' "IN-047T"',sep = "")
				system(command)
				recordTimeMaster("IN-047T","TwilioAlert",as.character(dataread[idx,1]))
				sendEmailToo(message,"IN-047T Meter error")
				doneforthedayfiringpac <<-1
				print(paste('Pac less than threshold error, message fired, time',as.character(dataread[idx,1])))
				lastfiredtspac <<- Sys.time()
			}
		}
	}	
	}
	if(!doneforthedayfiringpac)
		print(paste('Twilio passed for pac values',as.character(dataread[nrow(dataread),1])))
}
datepreviousouter = " "
managebacklog = function()
{
	today = as.character(Sys.Date())
	nowdate = unlist(strsplit(as.character(today),"-"))
	yrnow = as.numeric(nowdate[1])
	mnnow = as.numeric(nowdate[2])
	dynow = as.numeric(nowdate[3])
	print(paste('Now',yrnow,mnnow,dynow))
	lastdate = readLines(pathlastday)
	newdate = unlist(strsplit(as.character(lastdate),"-"))
	yrlast = as.numeric(newdate[1])
	mnlast = as.numeric(newdate[2])
	dylast = as.numeric(newdate[3])
	print(paste('Last',yrlast,mnlast,dylast))
	{
		if((yrnow==yrlast) && (mnnow==mnlast) && ((dynow -dylast) <= 1))
		{
			print('No Backlogs')
			return()
		}
		else if((yrnow==yrlast) && ((mnnow-1)==mnlast) && (dynow==1) && (dylast == daysmonthsno[mnlast]))
		{
				print('No Backlogs')
				return()
		}
	}
	print('backlog exist')
	print(paste('last day:',lastdate,'today:',today))
	for(x in yrlast : yrnow)
	{
		pathyrraw = paste(pathraw,x,sep="/")
		if(!file.exists(pathyrraw))
			dir.create(pathyrraw)
		pathyrgen1 = paste(pathgen1,x,sep="/")
		if(!file.exists(pathyrgen1))
			dir.create(pathyrgen1)
		for(y in mnlast : mnnow)
		{
			mnlast2 = paste(x,"-",y,sep="")
			if(y < 10)
			{
				mnlast2 = paste(x,"-0",y,sep="")
			}
			pathmonraw = paste(pathyrraw,mnlast2,sep="/")
			print(pathmonraw)
			if(!file.exists(pathmonraw))
				dir.create(pathmonraw)
			pathmongen1 = paste(pathyrgen1,mnlast2,sep="/")
			if(!file.exists(pathmongen1))
				dir.create(pathmongen1)
			start = end = 1
			if(y == mnlast)
			{
				start = dylast + 1
			}
			{
				if(y == mnnow)
				{
					end = dynow-1
				}
				else
				{
					end = daysmonthsno[y]
				}
			}
			for(z in start : end)
			{
				day = z
				mon = y
				yr = x
				if(day < 10)
				{
					day = paste("0",day,sep="")
				}
				if(mon < 10)
				{
					mon = paste("0",mon,sep="")
				}
				day1 = paste(day,mon,yr,sep="/")
				dayac = paste(yr,mon,day,sep="-")
				pathdayraw = paste(pathmonraw,"/",stationno," ",dayac,".txt",sep="")
				pathdaygen1 = paste(pathmongen1,"/",stationno," ",dayac,".txt",sep="")
				df = fetchrawdata(day1,day1)
				write.table(df,file = pathdayraw,row.names = F,col.names = T,sep = "\t",append = F)
				df2 = cleansedata(pathdayraw)
				write.table(df2,file = pathdaygen1,row.names = F,col.names = T,sep = "\t",append = F)
				write(dayac,file=pathlastday,append = F)
				print(paste(dayac,"done"))
			}
		}
	}
	datepreviousouter <<- dayac
}

manageflags = function()
{
	tmnow = Sys.time()
	if(doneforthedayfiringpac)
	{
		print('Checking if pac flag has to be reset')
		print(paste('time pac flag set',as.character(lastfiredtspac),'Time now',as.character(tmnow)))
		{
			if(abs(as.numeric(difftime(tmnow,lastfiredtspac,units = "mins")))> 60)
			{
				print('Time is more than 1 hr so resetting flag pac')
				doneforthedayfiringpac <<-0
			}
			else
				print('Time not yet elspased, so not resetting flag')
		}
	}
	print(paste('Manage flag operations done, donefiring pac is ',doneforthedayfiringpac))
}

dayac = datepreviousouter
#req = httr::POST("http://52.70.243.223/torp/ServiceRouter/login?loginid=operations@cleantechsolar.com&pwd=torp1227")
managebacklog()
ctrbackoff = 0
dayprevac = dayac
while(1)
{
	time = Sys.time()
	time = format(time, tz="Asia/Calcutta",usetz=TRUE)
	time = as.character(time)
	time = unlist(strsplit(time,"\\ "))
	date = time[1]
	time = time[2]
	timeac = timetomins(time)
	if(timeac < 120)
	{
		print('In doze mode')
		ctrbackoff = 12
		Sys.sleep(3600)
		next
	}
	if(ctrbackoff > 11)
	{
		print('Hit backoff making connection request...')
#		req = httr::POST("http://52.70.243.223/torp/ServiceRouter/login?loginid=operations@cleantechsolar.com&pwd=torp1227")
		ctrbackoff = 0
		print('request done')
	}
	rfmt = unlist(strsplit(date,"-"))
	yr = rfmt[1]
	mnth = rfmt[2]
	day = rfmt[3]
	day1 = paste(rfmt[3],rfmt[2],rfmt[1],sep="/")
	pathyrraw = paste(pathraw,yr,sep="/")
	if(!file.exists(pathyrraw))
		dir.create(pathyrraw)
	mnth2 = paste(yr,mnth,sep="-")
	pathmonthraw = paste(pathyrraw,mnth2,sep="/")
	if(!file.exists(pathmonthraw))
		dir.create(pathmonthraw)
	pathwriteraw = paste(pathmonthraw,"/",stationno," ",date,".txt",sep="")
	pathyrgen1 = paste(pathgen1,yr,sep="/")
	if(!file.exists(pathyrgen1))
		dir.create(pathyrgen1)
	pathmonthgen1 = paste(pathyrgen1,mnth2,sep="/")
	if(!file.exists(pathmonthgen1))
		dir.create(pathmonthgen1)
	pathwritegen1 = paste(pathmonthgen1,"/",stationno," ",date,".txt",sep="")
	print('Making fetch call')
	df = fetchrawdata(day1,day1)
	{
	if((!is.null(df)) && (nrow(df) > 0))
	{
	print(paste('Fetched raw data for',day1))
	write.table(df,file = pathwriteraw,row.names = F,col.names = T,sep = "\t",append = F)
	df2 = cleansedata(pathwriteraw)
	lsttime = as.character(df2[nrow(df2),1])
	write.table(df2,file = pathwritegen1,row.names = F,col.names = T,sep = "\t",append = F)
	write(date,file=pathlastday,append = F)
	print(paste('Iteration done last timestamp',lsttime))
	manageflags()
	firetwilio(pathwritegen1)
	}
	else
	{
		print(paste('No data there for',day1))
	}
	}
	Sys.sleep(300)
	ctrbackoff = ctrbackoff + 1
	if(dayprevac == " ")
	{
		print('Yes dateprevac not initialized so doing it')
		dayprevac = date
	}
	if(date==dayprevac)
	{
		next
	}
	print('Moving on to the next day....')
	dayprevac = date
	doneforthedayfiringpac <<- 0
	lastsuccesspacts <<- " "
}
