source('/home/admin/CODE/common/aggregate.R')

METERACNAMESAGG = c("DI_B_MFM","PR_B_MFM","TE_C_MFM","UP_CR_MFM","DI_B_Inverter_1","DI_B_Inverter_2","DI_B_Inverter_3","DI_B_Inverter_4","DI_B_Inverter_5","PR_B_Inverter_1","PR_B_Inverter_2","PR_B_Inverter_3","PR_B_Inverter_4","PR_B_Inverter_5","TE_C_Inverter_1","TE_C_Inverter_2","UP_CR_Inverter_1", "UP_CR_Inverter_2")

registerMeterList("IN-016C",METERACNAMESAGG)
for( x in 1 : length(METERACNAMESAGG))
{
		aggNameTemplate = getNameTemplate()
		aggColTemplate = getColumnTemplate()
		{
		if(grepl("WMS",METERACNAMESAGG[x]))
		{
			aggColTemplate[1] = 1 #Column no for date
			aggColTemplate[2] = 2 #Column no for DA
			aggColTemplate[3] = NA #column for LastRead
			aggColTemplate[4] = NA #column for LastTime
			aggColTemplate[5] = NA #column for Eac-1
			aggColTemplate[6] = NA #column for Eac-2
			aggColTemplate[7] = NA #column for Yld-1
			aggColTemplate[8] = NA #column for Yld-2
			aggColTemplate[9] = NA #column for PR-1
			aggColTemplate[10] = NA #column for PR-2
			aggColTemplate[11] = 3 #column for Irr
			aggColTemplate[12] = "Self" # IrrSrc Value
			aggColTemplate[13] = 4 #column for Tamb
			aggColTemplate[14] = 5 #column for Tmod
			aggColTemplate[15] = NA #column for Hamb
		}
		else if(grepl("MFM",METERACNAMESAGG[x]))
		{
			aggColTemplate[1] = 1 #Column no for date
			aggColTemplate[2] = 2 #Column no for DA
			aggColTemplate[3] = 9 #column for LastRead
			aggColTemplate[4] = 10 #column for LastTime
			aggColTemplate[5] = 3 #column for Eac-1
			aggColTemplate[6] = 4 #column for Eac-2
			aggColTemplate[7] = 5 #column for Yld-1
			aggColTemplate[8] = 6 #column for Yld-2
			aggColTemplate[9] = 7 #column for PR-1
			aggColTemplate[10] = 8 #column for PR-2
			aggColTemplate[11] = NA #column for Irr
			aggColTemplate[12] = "IN-712S-WMS" # IrrSrc Value
			aggColTemplate[13] = NA #column for Tamb
			aggColTemplate[14] = NA #column for Tmod
			aggColTemplate[15] = NA #column for Hamb
			#aggColTemplate[16] = NA #column for IA
			#aggColTemplate[17] = 11 #column for GA
			#aggColTemplate[18] = 12 #column for PA
		}
		else if(grepl("Inverter",METERACNAMESAGG[x]))
		{
			aggColTemplate[1] = 1 #Column no for date
			aggColTemplate[2] = 2 #Column no for DA
			aggColTemplate[3] = 8 #column for LastRead
			aggColTemplate[4] = 9 #column for LastTime
			aggColTemplate[5] = 3 #column for Eac-1
			aggColTemplate[6] = 4 #column for Eac-2
			aggColTemplate[7] = 6 #column for Yld-1
			aggColTemplate[8] = 7 #column for Yld-2
			aggColTemplate[9] = NA #column for PR-1
			aggColTemplate[10] = NA #column for PR-2
			aggColTemplate[11] = NA #column for Irr
			aggColTemplate[12] = NA # IrrSrc Value
			aggColTemplate[13] = NA #column for Tamb
			aggColTemplate[14] = NA #column for Tmod
			aggColTemplate[15] = NA #column for Hamb
			#aggColTemplate[16] = 10 #column for IA
		}
		}
		registerColumnList("IN-016C",METERACNAMESAGG[x],aggNameTemplate,aggColTemplate)
}
